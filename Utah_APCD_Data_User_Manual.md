# Utah All-Payer Claims Database (APCD) Data User Manual

February 2022

## Introduction

The Utah Health Data Committee (HDC) is composed of fifteen governor-appointed members and was created by § 26-33a, Utah Health Data Authority Act. The Committee is staffed by the Office of Health Care Statistics (OHCS) which manages the Utah All Payers Claim Database (APCD).

Utah Administrative Rule (R428-2) requires all Utah carriers that cover 2,500 or more (20,000 or more for stand-alone dental carriers) Utah residents to submit to the Utah APCD. Four different file types are submitted: eligibility, medical, pharmacy, and provider. These files contain information on patient demographics and eligibility, services or medications received, financial elements, diagnoses and claim specific elements, and provider information.

Data submissions by the carriers are based on claim adjudication dates. As such, the database is constantly being updated as new claims are adjudicated and submitted to the Utah APCD. Data accuracy is reliant on completeness of the claims submitted. Continual efforts are being made to further data completeness and accuracy.

You may obtain information how to submit a data request and view associated pricing from the OHCS site at: https://stats.health.utah.gov/about-the-data/data-series/. Please ensure that all required fields and documents are submitted to ensure timely processing of the data request. Additional information regarding the APCD is available on the OHCS at:  https://stats.health.utah.gov/about/frequently-asked-questions/. Additional inquires can be submitted to healthcarestat@utah.gov.


# Utah All-Payer Claims Database (APCD) Data User Manual

February 2021

## Table of Contents

- [Available Data](#available-data)
  - [DIM_MEMBER](#dim_member)
  - [FACT_MEMBER_MONTHS](#fact_member_months)
  - [FACT_SERVICES](#fact_services)

## Available Data

There are three basic tables in the Utah APCD:


- `DIM_MEMBER` - which contains basic, unchanging member demographic information;
- `FACT_MEMBER_MONTHS` - which contains basic insurance plan information at the month
  level; and
- `FACT_SERVICES` - which contains basic claims information at the claim-line level.

The basic connection between the three tables is the `MEMBER_KEY`.

### DIM_MEMBER

Member-level variables.

|   Variable Name    |                                                          Description                                                           |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------ |
| MEMBER_KEY         | Milliman created member ID                                                                                                     |
| MI_PERSON_KEY_HASHED| Longitudinal person identifier                                                                                                 |
| MEM_GENDER         | Member gender                                                                                                                  |
| MEM_RACE           | Member race. <br>R1 = American Indian/Alaska Native<br>R2 = Asian<br>R3 = Black/African American<br>R4 = Native Hawaiian or other Pacific <br>R5 = White<br>R9 = Other Race<br>UNKNOW/Blank = Unknown/Not Specified       |
| MEM_ETHNICITY      | Member ethnicity.<br>Y = Patient is Hispanic/Latino/Spanish<br>N = Patient is not Hispanic/Latino/Spanish<br>U/Blank = Unknown |
| MEM_DOB            | Member date of birth                                                                                                           |
| MEM_DOB_YEAR       | Member year of birth                                                                                                           |
| MEM_START_DATE	   | Member start date                                                                                                              |
|MEM_START_YEAR	     | Member start year                                                                                                              |
|MEM_END_DATE	       | Member end date                                                                                                                |
|MEM_END_YEAR	       | Member end year                                                                                                                |
|MEM_CITY	           | Member city                                                                                                                    |
|MEM_COUNTY	         | Member county                                                                                                                  |
|MEM_STATE	         | Member state                                                                                                                   |
|MEM_ZIP	           | Member zip code                                                                                                                |
|CMS_MEDICARE_FLAG	 | Flag identifying Medicare (Parts A, B, & D) data (Additional approval needed)                                                  |
|MEDICAID_FLAG	     | Flag identifying Medicaid data (Additional approval needed)                                                                    |

### FACT_MEMBER_MONTHS

Member-month level variables.

|        Variable Name        |                                                                                                                Description                                                                                                                 |
| --------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| MEMBER_KEY                  | Milliman created member ID                                                                                                                                                                                                                 |
| MEMBER_MONTH_START_DATE_INT | Start of month for given member month relationship                                                                                                                                                                                         |
| QTY_MM_MD                   | Variable indicating member month medical eligibility                                                                                                                                                                                       |
| QTY_MM_RX                   | Variable indicating member month pharmacy eligibility                                                                                                                                                                                      |
| QTY_MM_DN                   | Variable indicating member month dental eligibility                                                                                                                                                                                        |
| INSURANCE_TYPE_CODE_ENR     | Insurance product type code for eligibility. See [Lookup Table B-1.A](https://gitlab.com/UtahOHCS/APCD_DSG/-/blob/master/APCD_UT_DSG.md#b-1a-insurance-type)                                                                               |
| PRIMARY_INS_IND             | Primary insurance indicator                                                                                                                                                                                                                |
| COVG_LEVEL                  | Benefit coverage level. See [Lookup Table B-1.B](https://gitlab.com/UtahOHCS/APCD_DSG/-/blob/master/APCD_UT_DSG.md#b-1b-coverage-level-code).                                                                                              |
| HIGH_DED_HSA                | High dedcutible or health savings account plan flag                                                                                                                                                                                        |
| METL_VALUE                  | Metal Level (percentage of Actuarial Value) per federal regulations. Valid values are: <br>1 - Platinum <br>2 - Gold <br>3 - Silver <br>4 - Bronze <br>5 - Catastrophic <br>0 - Not Applicable                                             |
| COVG_TYPE                   | Insurance coverage type.<br> STN - short-term, non-renewable health insurance (ie COBRA) <br>UND - plans underwritten by the insurer <br>OTH - any other plan. Insurers using this code shall obtain prior approval. <br>AWS - Self-funded |
| RISK_BASIS                  | Risk Basis indicator (self-insured or fully insured)<br> S - Self-insured <br>F - Fully insured                                                                                                                                            |
| CMS_MEDICARE_DATA_FLAG      | Flag identifying CMS Medicare data                                                                                                                                                                                                         |
| MEDICAID_DATA_FLAG          | Flag identifying Medicaid data                                                                                                                                                                                                             |

### FACT_SERVICES

Claim-level variables. Some of these variables are "normalized" into lookup tables for
storage efficiency purposes but are listed here for completeness.

|        Variable Name        |                                                                      Description                                                                       |
| --------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ |
| CLAIM_ID_KEY                | Milliman created claim ID                                                                                                                              |
| SERVICES_KEY                | Milliman created services ID                                                                                                                           |
| MEMBER_KEY                  | Milliman created member ID                                                                                                                             |
| INSURANCE_TYPE_CODE_CLAIM   | Insurance product type code for claim. See [Lookup Table B-1.A](https://gitlab.com/UtahOHCS/APCD_DSG/-/blob/master/APCD_UT_DSG.md#b-1a-insurance-type) |
| FORM_TYPE                   | Identifies type of claim.<br> U = UB<br> H = CMS1500<br> D = Prescription Drug<br> A = Dental<br> V = Vision<br> L = Lab                               |
| UB_BILL_TYPE_FACILITY_TYPE  | Identifies type of facility base on UB Bill Type Code                                                                                                  |
| UB_BILL_TYPE_CLASS          | Classification of Bill Type Code                                                                                                                       |
| UB_BILL_TYPE_DESC           | Description of Bill Type Code                                                                                                                          |
| UB_BILL_TYPE_ID             | Bill Type Code                                                                                                                                         |
| POS_LABEL                   | Place of Service                                                                                                                                       |
| FROM_DATE_INT               | First date of service for this service line. Displayed as an integer                                                                                   |
| TO_DATE_INT                 | Last date of service for this service line. Displayed as an integer                                                                                    |
| ADM_DATE_INT                | Admission date. Displayed as an integer                                                                                                                |
| DIS_DATE_INT                | Discharge date. Displayed as an integer                                                                                                                |
| ADM_SRC                     | Admission Source Code                                                                                                                                  |
| ADM_TYPE                    | Admission Type Code                                                                                                                                    |
| DIS_STAT                    | Discharge Status Code                                                                                                                                  |
| DIS_STAT_DESC               | Discharge Status Code Description                                                                                                                      |
| SERVICE_LINE                | Line number for this service                                                                                                                           |
| REV_CODE_CODE               | Revenue Code                                                                                                                                           |
| REV_CODE_DESC               | Revenue Code Description                                                                                                                               |
| PROC_CODE                   | HCPCS/CPT Code                                                                                                                                         |
| PROC_DESC                   | HCPCS/CPT Code Description                                                                                                                             |
| PROC_MOD1                   | HCPCS/CPT Code Modifier 1                                                                                                                              |
| PROC_MOD1_DESC              | HCPCS/CPT Code Modifier 1 Description                                                                                                                  |
| PROC_MOD2                   | HCPCS/CPT Code Modifier 2                                                                                                                              |
| PROC_MOD2_DESC              | HCPCS/CPT Code Modifier 2 Description                                                                                                                  |
| PROC_MOD3                   | HCPCS/CPT Code Modifier 3                                                                                                                              |
| PROC_MOD3_DESC              | HCPCS/CPT Code Modifier 3 Description                                                                                                                  |
| PROC_MOD4                   | HCPCS/CPT Code Modifier 4                                                                                                                              |
| PROC_MOD4_DESC              | HCPCS/CPT Code Modifier 4 Description                                                                                                                  |
| QTY_SVC_UNITS               | Service units                                                                                                                                          |
| UNIT_OF_MEASURE             | DA – Days <br>MJ – Minutes <br>UN – Units <br>Other standard ANSI values may be used.                                                                  |
| RX_UNIT_OF_MEASURE          | Pharmacy mesurement unit. See [Lookup Table B-1.K](https://gitlab.com/UtahOHCS/APCD_DSG/-/blob/master/APCD_UT_DSG.md#b-1k-drug-unit-of-measure) for valid values. |
| RX_NDC_CODE                 | National Drug Code                                                                                                                                     |
| RX_NDC_PROD_NAME            | National Drug Code Product Name is the name of the drug that is associated with the unique product identifier for drugs                                |
| QTY_RX_DAYS_SUPPLY          | Prescription Days’ Supply is the number of days that the drug will last if taken at the prescribed dose                                                |
| RX_DAW                      | Dispensed as written code                                                                                                                              |
| RX_REFILLS                  | Prescription Refills is the count of times the prescription has been filled                                                                            |
| RX_COMPOUND_IND             | Compound drug indicator                                                                                                                                |
| CAPITATED_SERVICE_INDICATOR | Y – Services are paid under a capitated arrangement <br>N – Services are not paid under a capitated arrangement <br>U – Unknown                        |
| AMT_BILLED                  | Charge/Billed Amount.                                                                                                                                  |
| AMT_ALLOWED                 | Allowed/Negotiated/Contracted Amount.                                                                                                                  |
| AMT_PAID                    | Amount paid by the insurance plan                                                                                                                      |
| AMT_COPAY                   | The mututally excludsive preset, fixed dollar amount for which the individual is responsible                                                           |
| AMT_DEDUCT                  | The mututally excludsive amount member is liable for that goes towards member deductible                                                               |
| AMT_COINS                   | The mututally excludsive coinsurance amount the member is liable for                                                                                   |
| AMT_PREPAID                 | For capitated services, the fee for service equivalent amount                                                                                          |
| RATE_RX_INGR_COST           | Prescription Ingredient Cost is the amount in dollars of the cost of the ingredients that are contained in the drug                                    |
| AMT_RX_DISP_FEE             | Prescription Dispensing Fee is the amount in dollars that the pharmacist charged for dispensing the drug.                                              |
| ICD_DIAG_01                 | ICD Diagnosis Code 1                                                                                                                                   |
| ICD_DIAG_01_TYPE            | ICD Diagnosis Code 1 Type (ICD-9-CM or ICD-10-CM)                                                                                                      |
| ICD_DIAG_01_DESC            | ''                                                                                                                                                     |
| ICD_DIAG_02                 | ''                                                                                                                                                     |
| ICD_DIAG_02_TYPE            | ''                                                                                                                                                     |
| ICD_DIAG_02_DESC            | ''                                                                                                                                                     |
| ICD_DIAG_03                 | ''                                                                                                                                                     |
| ICD_DIAG_03_TYPE            | ''                                                                                                                                                     |
| ICD_DIAG_03_DESC            | ''                                                                                                                                                     |
| ICD_DIAG_04                 | ''                                                                                                                                                     |
| ICD_DIAG_04_TYPE            | ''                                                                                                                                                     |
| ICD_DIAG_04_DESC            | ''                                                                                                                                                     |
| ICD_DIAG_05                 | ''                                                                                                                                                     |
| ICD_DIAG_05_TYPE            | ''                                                                                                                                                     |
| ICD_DIAG_05_DESC            | ''                                                                                                                                                     |
| ICD_DIAG_06                 | ''                                                                                                                                                     |
| ICD_DIAG_06_TYPE            | ''                                                                                                                                                     |
| ICD_DIAG_06_DESC            | ''                                                                                                                                                     |
| ICD_DIAG_07                 | ''                                                                                                                                                     |
| ICD_DIAG_07_TYPE            | ''                                                                                                                                                     |
| ICD_DIAG_07_DESC            | ''                                                                                                                                                     |
| ICD_DIAG_08                 | ''                                                                                                                                                     |
| ICD_DIAG_08_TYPE            | ''                                                                                                                                                     |
| ICD_DIAG_08_DESC            | ''                                                                                                                                                     |
| ICD_DIAG_09                 | ''                                                                                                                                                     |
| ICD_DIAG_09_TYPE            | ''                                                                                                                                                     |
| ICD_DIAG_09_DESC            | ''                                                                                                                                                     |
| ICD_DIAG_10                 | ''                                                                                                                                                     |
| ICD_DIAG_10_TYPE            | ''                                                                                                                                                     |
| ICD_DIAG_10_DESC            | ''                                                                                                                                                     |
| ICD_PROC_01                 | ICD Procedure Code 1                                                                                                                                   |
| ICD_PROC_01_TYPE            | ICD Procedure Code 1 Type (ICD-9-CM or ICD-10-PCS)                                                                                                     |
| ICD_PROC_01_DESC            | ''                                                                                                                                                     |
| ICD_PROC_02                 | ''                                                                                                                                                     |
| ICD_PROC_02_TYPE            | ''                                                                                                                                                     |
| ICD_PROC_02_DESC            | ''                                                                                                                                                     |
| ICD_PROC_03                 | ''                                                                                                                                                     |
| ICD_PROC_03_TYPE            | ''                                                                                                                                                     |
| ICD_PROC_03_DESC            | ''                                                                                                                                                     |
| ICD_PROC_04                 | ''                                                                                                                                                     |
| ICD_PROC_04_TYPE            | ''                                                                                                                                                     |
| ICD_PROC_04_DESC            | ''                                                                                                                                                     |
| DRG_TYPE                    | DRG Type (Always MS-DRG)                                                                                                                               |
| DRG_CODE                    | MS-DRG Code.                                                                                                                                           |
| DRG_DESC                    | MS-DRG Description.                                                                                                                                    |
| MDC_CODE                    | Major Diagnostic Category.                                                                                                                             |
| MDC_DESC                    | Major Diagnostic Category Description.                                                                                                                 |
| TOOTH_NUMBER                | Tooth Number or Letter Identification                                                                                                                  |
| AREA_CAVITY                 | Area of Oral Cavity code as maintained by the American Dental Association                                                                              |
| TOOTH_SURFACE               | Tooth Surface Identification                                                                                                                           |
| SRVC_PROV_TAX_ID            | Service Provider Tax ID Number                                                                                                                         |
| SRVC_PROV_NATL_PROV_ID      | Service Provider NPI                                                                                                                                   |
| SRVC_PROV_FIRST_NAME        | Service Provider First Name                                                                                                                            |
| SRVC_PROV_MID_NAME          | Service Provider Middle Name                                                                                                                           |
| SRVC_PROV_LAST_NAME         | Service Provider Last Name                                                                                                                             |
| SRVC_PROV_STREET_ADDR       | Service Provider Street Address                                                                                                                        |
| SRVC_PROV_CITY              | Service Provider City                                                                                                                                  |
| SRVC_PROV_STATE             | Service Provider State                                                                                                                                 |
| SRVC_PROV_ZIP               | Service Provider Zip Code                                                                                                                              |
| BILL_PROV_NPI               | Billing Provider NPI                                                                                                                                   |
| BILL_PROV_LAST_NAME         | Full name of provider billing organization or last name of individual billing provider                                                                 |
| PHARM_TAX_ID                | Pharmacy Tax ID Number                                                                                                                                 |
| PHARM_NPI                   | Pharmacy NPI                                                                                                                                           |
| PHARM_NAME                  | Pharmacy Name                                                                                                                                          |
| PHARM_STREET_ADDR           | Pharmacy Street Address                                                                                                                                |
| PHARM_CITY                  | Pharmacy City                                                                                                                                          |
| PHARM_STATE                 | Pharmacy State                                                                                                                                         |
| PHARM_ZIP                   | Pharmacy Zip Code                                                                                                                                      |
| PRESCR_PHYS_NPI             | Prescribing Physician NPI                                                                                                                              |
| PRESCR_PHYS_FIRST_NAME      | Prescribing Physician First Name                                                                                                                       |
| PRESCR_PHYS_MID_NAME        | Prescribing Physician Middle Name                                                                                                                      |
| PRESCR_PHYS_LAST_NAME       | Prescribing Physician Last Name                                                                                                                        |
| CMS_MEDICARE_DATA_FLAG      | Flag identifying CMS Medicare data                                                                                                                     |
| MEDICAID_DATA_FLAG          | Flag identifying Medicaid data                                                                                                                         |
